import React from "react";
import { connect } from "react-redux";
import { XOA_GHE } from "../Reducers/Constants";

function Table(props) {
  let { danhSachGheChon, formInfo } = props;
  const renderTbody = () => {
    return danhSachGheChon.map((ghe, index) => {
      return (
        <tr key={index}>
          <td>{ghe.soGhe}</td>
          <td>{ghe.gia}</td>
          <td>
            <button
              onClick={() => props.xoaGhe(ghe)}
              className="btn btn-danger"
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };
  const tongTien = () => {
    return danhSachGheChon.reduce((sum, item) => {
      return (sum += item.gia);
    }, 0);
  };
  tongTien();
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Tên khách hàng: {formInfo.ten}</th>
          <th> Số ghế đặt: {formInfo.soGheChon}</th>
          <th></th>
        </tr>
        <tr>
          <th> Số ghế</th>
          <th>Giá</th>
          <th>Thao tác</th>
        </tr>
      </thead>
      <tbody>{renderTbody()}</tbody>
      <tfoot>
        <tr>
          <td colSpan={2}>
            <h5 className="font-weight-bold">Tổng Tiền</h5>
          </td>
          <td>
            <h5 className="font-weight-bold">{tongTien()}</h5>
          </td>
        </tr>
      </tfoot>
    </table>
  );
}

const mapStateToProps = (state) => {
  return {
    danhSachGheChon: state.BaiTapDatVeReducer.danhSachGheChon,
    formInfo: state.BaiTapDatVeReducer.formInfo,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    xoaGhe: (ghe) => {
      dispatch({
        type: XOA_GHE,
        payload: ghe,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Table);
