import React, { useState } from "react";
import { connect } from "react-redux";
import { danhSachGhe } from "../SourceFiles/danhSachGhe";
import { XAC_NHAN_CHON_GHE } from "../Reducers/Constants";

function DanhSachGhe(props) {
  let { formInfo } = props;
  const [danhSachGheChon, setDanhSachGheChon] = useState([]);
  // const handleChonGhe = (ghe) => {
  //   const soGheChon = formInfo.soGheChon;
  //   if (danhSachGheChon.length < soGheChon) {
  //     let index = danhSachGheChon.findIndex((item) => item.soGhe === ghe.soGhe);
  //     if (index !== -1) {
  //       danhSachGheChon.splice(index, 1);
  //       setDanhSachGheChon(danhSachGheChon);
  //     } else {
  //       danhSachGheChon.push({ ...ghe, daDat: true });
  //       setDanhSachGheChon(danhSachGheChon);
  //     }
  //   } else {
  //     let index = danhSachGheChon.findIndex((item) => item.soGhe === ghe.soGhe);
  //     if (index !== -1) {
  //       danhSachGheChon.splice(index, 1);
  //       setDanhSachGheChon(danhSachGheChon);
  //     } else {
  //       return;
  //     }
  //   }
  //   console.log(danhSachGheChon);
  // };

  const handleChonGhe = (e, ghe) => {
    const soGheChon = formInfo.soGheChon;
    if (danhSachGheChon.length < soGheChon) {
      let index = danhSachGheChon.findIndex((item) => item.soGhe === ghe.soGhe);
      if (index !== -1) {
        e.target.classList.remove("gheDangChon");
        danhSachGheChon.splice(index, 1);
        setDanhSachGheChon(danhSachGheChon);
      } else {
        e.target.classList.add("gheDangChon");
        danhSachGheChon.push({ ...ghe, daDat: true });
        setDanhSachGheChon(danhSachGheChon);
      }
    } else {
      let index = danhSachGheChon.findIndex((item) => item.soGhe === ghe.soGhe);
      if (index !== -1) {
        e.target.classList.remove("gheDangChon");
        danhSachGheChon.splice(index, 1);
        setDanhSachGheChon(danhSachGheChon);
      } else {
        return;
      }
    }
  };

  const xacNhanChonGhe = () => {
    const soGheChon = formInfo.soGheChon;
    if (danhSachGheChon.length != soGheChon) {
      alert("Chưa chọn đúng số ghế!");
      return;
    } else {
      props.sendDanhSachGhe(danhSachGheChon);
    }
  };

  const renderDanhSachGhe = () => {
    return danhSachGhe.map(({ hang, danhSachGhe }, index) => {
      return (
        <div key={index}>
          <span className="firstChar">{hang}</span>
          {danhSachGhe.map((ghe, index) => {
            return ghe.daDat === false ? (
              <button
                key={index}
                className="ghe p-0 m-1"
                onClick={(e) => {
                  handleChonGhe(e, ghe);
                }}
              >
                {ghe.soGhe}
              </button>
            ) : (
              <button key={index} className="ghe gheDuocChon m-1">
                {ghe.soGhe}
              </button>
            );
          })}
        </div>
      );
    });
  };

  return (
    <div className="seat-map">
      <div>{renderDanhSachGhe()}</div>
      <button className="btn btn-success mt-3" onClick={() => xacNhanChonGhe()}>
        Xac Nhan Dat Ghe
      </button>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    formInfo: state.BaiTapDatVeReducer.formInfo,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    sendDanhSachGhe: (danhSachGheChon) => {
      dispatch({
        type: XAC_NHAN_CHON_GHE,
        payload: danhSachGheChon,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DanhSachGhe);
