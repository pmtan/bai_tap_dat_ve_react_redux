import React, { useState } from "react";
import { connect } from "react-redux";
import { SUBMIT_FORM } from "../Reducers/Constants";

function Form(props) {
  const [values, setValues] = useState({ ten: "", soGheChon: null });
  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmitForm = () => {
    if (values.ten === "" || values.soGheChon === null) {
      alert("Vui lòng điền đầy đủ thông tin!");
      return;
    } else {
      props.sendData(values);
    }
  };
  return (
    <div className="container pt-4">
      <form className="form-inline justify-content-between">
        <label>
          <h5>Họ tên:</h5>
        </label>
        <input
          type="text"
          className="form-control col-4"
          name="ten"
          onChange={handleOnChange}
        />
        <label>
          <h5>Số Ghế Chọn:</h5>
        </label>
        <input
          type="text"
          className="form-control col-2"
          name="soGheChon"
          onChange={handleOnChange}
        />
        <button
          onClick={handleSubmitForm}
          type="button"
          className="btn btn-success col-3"
        >
          Bắt Đầu Chọn Ghế
        </button>
      </form>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    sendData: (data) => {
      dispatch({
        type: SUBMIT_FORM,
        payload: data,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(Form);

// if (state.values[e.target.name] === "") {
//   state.errors[e.target.name] =
//     e.target.name + " khong duoc bo trong.";
// }
// console.log(cloneState);
// setState(cloneState);
// };
// console.log(state);
// const inputSubmit = (values) =>{
//   if (inputs.ten == "" || inputs.soGheChon= ""){
//     alert("Vui long dien day du thong tin.");
//     return;
//   } else {
//     props.submitData(values);
//   }
// }

/* <form className="d-flex row">
        <div className="form-group col-5 d-flex align-item-center justify-content-between">
          <label>
            <h5>Ten: </h5>
          </label>
          <input
            type="text"
            className="form-control"
            onChange={handleOnChange}
            required
          />
        </div>
        <small className="form-text text-danger">error ten</small>
        <div className="form-group col-4 d-flex">
          <label className="col-6">
            <h5>So Ghe Chon: </h5>
          </label>
          <input
            type="number"
            className="form-control"
            name="soGheChon"
            onChange={handleOnChange}
            required
          />
          <small className="form-text text-danger">errorghe</small>
        </div>
        <button
          type="button"
          className="btn btn-success col-2"
          onClick={() => props.submitForm(state)}
        >
          Bat Dau Chon Ghe
        </button>
      </form> */
