import React from "react";
import DanhSachGhe from "./DanhSachGhe";
import Form from "./Form";
import "../SourceFiles/BaiTapBookingTicket.css";
import Table from "./Table";

export default function BaiTapDatVe() {
  return (
    <div className="background">
      <div className="seat-selection-background">
        <Form />
        <div className="screen pt-3"></div>
        <DanhSachGhe />
      </div>
      <div className="table-background">
        <Table />
      </div>
    </div>
  );
}
