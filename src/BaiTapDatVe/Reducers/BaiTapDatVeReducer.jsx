import { SUBMIT_FORM, XAC_NHAN_CHON_GHE, XOA_GHE } from "./Constants";

const inititalState = {
  formInfo: {},
  danhSachGheChon: [],
};

export const BaiTapDatVeReducer = (
  state = inititalState,
  { type, payload }
) => {
  switch (type) {
    case SUBMIT_FORM: {
      return {
        ...state,
        formInfo: payload,
      };
    }
    // case CHON_GHE: {
    //   const soGheChon = state.formInfo.soGheChon;
    //   let cloneDanhSachGheChon = [...state.danhSachGheChon];
    //   if (state.danhSachGheChon.length < soGheChon) {
    //     let index = cloneDanhSachGheChon.findIndex(
    //       (item) => item.soGhe === payload.soGhe
    //     );
    //     if (index !== -1) {
    //       cloneDanhSachGheChon.splice(index, 1);
    //     } else {
    //       cloneDanhSachGheChon.push({ ...payload, daDat: true });
    //     }
    //     return {
    //       ...state,
    //       danhSachGheChon: cloneDanhSachGheChon,
    //     };
    //   } else {
    //     let index = cloneDanhSachGheChon.findIndex(
    //       (item) => item.soGhe === payload.soGhe
    //     );
    //     if (index !== -1) {
    //       cloneDanhSachGheChon.splice(index, 1);
    //     } else {
    //       return;
    //     }
    //     return {
    //       ...state,
    //       danhSachGheChon: cloneDanhSachGheChon,
    //     };
    //   }
    // }
    // case XAC_NHAN_CHON_GHE: {
    //   let cloneDanhSachGhe = [...state.danhSachGhe];
    //   for (let i = 0; i < payload.length; i++) {
    //     let indexHangGhe = cloneDanhSachGhe.findIndex(
    //       (hangGhe) => hangGhe.hang === payload[i].soGhe.slice(0, 1)
    //     );
    //     let indexGhe = cloneDanhSachGhe[indexHangGhe].danhSachGhe.findIndex(
    //       (ghe) => ghe.soGhe === payload[i].soGhe
    //     );
    //     cloneDanhSachGhe[indexHangGhe].danhSachGhe[indexGhe] = payload[i];
    //   }
    //   return {
    //     ...state,
    //     danhSachGhe: cloneDanhSachGhe,
    //   };
    // }

    case XAC_NHAN_CHON_GHE: {
      return {
        ...state,
        danhSachGheChon: payload,
      };
    }
    case XOA_GHE: {
      let cloneDanhSachGheChon = [...state.danhSachGheChon];
      let index = cloneDanhSachGheChon.findIndex(
        (item) => item.soGhe === payload.soGhe
      );
      if (index !== -1) {
        cloneDanhSachGheChon.splice(index, 1);
      }
      return {
        ...state,
        danhSachGheChon: cloneDanhSachGheChon,
      };
    }
    default: {
      return {
        ...state,
      };
    }
  }
};
